<?php


foreach (<array> as <value>){

}

// 1 - o elemento é atribuido a variável valor
// 2 - o bloco da instrução foreach será executado
// A cada ciclo um elemento do nosso array é atribuido a nossa varivel valor.

foreach (<array> as <key> => <value>) {
        //se lembrar que primeiro a chave depois o valor
}



//Exemplo de uma estrutura foreach simples

$lista = ["domingo", "segunda", "terca", "quarta", "quinta", "sexta", "sabado"]
foreach($lista as $chave => $valor){
    echo "$chave: $valor /n";
}

//ver depois o que é "/n"





//EXEMPLO DE => (separador de matrizes associativas)
$user_list = array(
    'josé' => 'apassword',
    'pedro' => 'secr3t'
);

foreach ($user_list as $user => $pass) {
    echo "{$user} password de josé é: {$pass}\n";
}
// O que deve sair: 
// "password de josé é: apassword"
// "password de pedro é: secr3t"